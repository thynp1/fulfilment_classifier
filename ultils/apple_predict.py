import pandas as pd
import numpy as np
import time
import requests
import ast

# define a request
def apple_json_request(order, missing_value, exclude_missing=False):
    '''
    Function convert each row of a Data Frame into APPLE input json
    
    Input:
    ------
    order: a dataframe of shape (1, n+) where n is the number of variables included in the request
    missing_values: value to use to replace missing values in order
    exclude_missing: if True, sends the request without variables that are equal to np.nan
    
    Output:
    -------
    JSON request
    '''

    jr = {
        "truck_model_id":
        int(order["truck_model"])
        if not pd.isnull(order["truck_model"]) else missing_value,
        "truck_quantity":
        int(order["truck_quantity"])
        if not pd.isnull(order["truck_quantity"]) else missing_value,
        "pickup_datetime":
        order["pickup_date"]
        if not str(pd.isnull(order["pickup_date"])) else missing_value,
        "dropoff_datetime":
        order["drop_date"]
        if not pd.isnull(order["drop_date"]) else missing_value,
        "is_tax_invoice_requested":
        bool(order["is_tax_invoice_requested"])
        if not pd.isnull(order["is_tax_invoice_requested"]) else missing_value,
        "is_whole_truck":
        bool(order["is_whole_truck"])
        if not pd.isnull(order["is_whole_truck"]) else missing_value,
        "truck_type_id":
        int(order["truck_type_id"])
        if not pd.isnull(order["truck_type_id"]) else missing_value,
        "cargo_weight":
        float(order["cargo_weight"])
        if not pd.isnull(order["cargo_weight"]) else missing_value,
        "cargo_length":
        float(order["cargo_length"])
        if not pd.isnull(order["cargo_length"]) else missing_value,
        "cargo_width":
        float(order["cargo_width"])
        if not pd.isnull(order["cargo_width"]) else missing_value,
        "cargo_height":
        float(order["cargo_height"])
        if not pd.isnull(order["cargo_height"]) else missing_value,
        "bidder_type":
        int(order["bidder_type"])
        if not pd.isnull(order["bidder_type"]) else missing_value,
        "pickup_area_id":
        int(order["pickup_area"])
        if not pd.isnull(order["pickup_area"]) else missing_value,
        "dropoff_area_id":
        int(order["drop_area"])
        if not pd.isnull(order["drop_area"]) else missing_value,
        "distance":
        float(order["distance"])
        if not pd.isnull(order["distance"]) else missing_value,
        "duration":
        float(order["duration"])
        if not pd.isnull(order["duration"]) else missing_value,
        "truck_weight":
        float(order["truck_weight"])
        if not pd.isnull(order["truck_weight"]) else missing_value
    }
    if exclude_missing:
        valid_keys = [key for key in jr if not pd.isnull(jr[key])]
        new_jr = {}
        for key in valid_keys:
            new_jr[key] = jr[key]
        return new_jr
    else:
        return jr
    
# this takes hours to run. Please use table below, already created with imputed values

def _get_prices(order, url='http://3.19.67.84:6526/predict'):
    """
    Get a price for an order from APPLE API
    
    Input:
    ------
    order: order input Data Frame shape (1, n+)
    url: API URL, default is 'http://3.14.146.185:8000/predict'
    
    Output:
    -------
    Price of an order
    """
    try:
        r = requests.post(url, json=apple_json_request(order, 0)).content
    except:
        return np.nan
    try:
        return ast.literal_eval(r.decode('utf-8'))['total_price']
    except:
        return np.nan
    
def get_prices(missing_prices, url='http://3.19.67.84:6526/predict'):
    """
    Get a price for an Data Frame from APPLE API
    
    Input:
    ------
    missing_prices: order input Data Frame shape (m, n)
    url: API URL, default is 'http://3.14.146.185:6526/predict'
    
    Output:
    -------
    Price of an order
    """
    prices = []

    for i in range(len(missing_prices)):
        order = missing_prices.iloc[i,:]
        prices.append(_get_prices(order, url))
        if i%100 == 0:
            print(f'{time.ctime(int(time.time()))}: predicted {i} prices')
    return prices

def _get_prices(order, url='http://3.19.67.84:6523/predict'):
    """
    Get a price for an order from APPLE API
    
    Input:
    ------
    order: order input Data Frame shape (1, n+)
    url: API URL, default is 'http://3.14.146.185:6523/predict'
    
    Output:
    -------
    Price of an order
    """
    try:
        r = requests.post(url, json=apple_json_request(order, 0)).content
    except:
        return np.nan
    try:
        return ast.literal_eval(r.decode('utf-8'))['total_price']
    except:
        return np.nan
    
def apple_get_prices(missing_prices, url='http://3.19.67.84:6523/predict'):
    """
    Get a price for an Data Frame from APPLE API
    
    Input:
    ------
    missing_prices: order input Data Frame shape (m, n)
    url: API URL, default is 'http://3.14.146.185:6523/predict'
    
    Output:
    -------
    Price of an order
    """
    prices = []

    for i in range(len(missing_prices)):
        order = missing_prices.iloc[i,:]
        prices.append(_get_prices(order, url))
        if i%1 == 0:
            print(f'{time.ctime(int(time.time()))}: predicted {i} prices')
    return prices