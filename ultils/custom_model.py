import torch
import torch.nn as nn
import torch.nn.functional as F



def customize_weight_init(x):
    classname = x.__class__.__name__
    if classname.find('Linear') != -1:
        nn.init.xavier_normal_(x.weight.data)
        nn.init.constant_(x.bias.data,0)
    if classname.find('BatchNorm') != -1:
        nn.init.constant_(x.weight, 1)
        nn.init.constant_(x.bias, 0)
    if classname.find('Embedding') != -1:
        nn.init.xavier_normal_(x.weight.data)

class NNModel(nn.Module):
    def __init__(self, emb_szs, n_cont, emb_drop, out_sz, szs, drops, use_bn=True):
        super().__init__()
        for i,(c,s) in enumerate(emb_szs):assert c > 1, f"cardinality must be >=2, got emb_szs[{i}]: ({c},{s})"
        self.embedding = nn.ModuleList([nn.Embedding(c, s) for c,s in emb_szs])
        
        n_emb = sum(e.embedding_dim for e in self.embedding)
        self.n_emb, self.n_cont=n_emb, n_cont
        
        szs = [n_emb+n_cont] + szs
        
        self.lins = nn.ModuleList([nn.Linear(szs[i], szs[i+1]) for i in range(len(szs)-1)])
        self.bns = nn.ModuleList([nn.BatchNorm1d(sz) for sz in szs[1:]])
        self.outp = nn.Linear(szs[-1], out_sz)

        self.emb_drop = nn.Dropout(emb_drop)
        self.drops = nn.ModuleList([nn.Dropout(drop) for drop in drops])
        self.bn = nn.BatchNorm1d(n_cont)
        self.use_bn= use_bn

    def forward(self, x_cat, x_cont):
        if self.n_emb != 0:
            x = [e(x_cat[:,i]) for i,e in enumerate(self.embedding)]
            x = torch.cat(x, 1)
            x = self.emb_drop(x)
            
        if self.n_cont != 0:
            x2 = self.bn(x_cont)
            x = torch.cat([x, x2], 1) if self.n_emb != 0 else x2
            
        for l,d,b in zip(self.lins, self.drops, self.bns):
            x = F.relu(l(x))
            if self.use_bn: x = b(x)
            x = d(x)
        x = self.outp(x)
        out = F.softmax(x)
        return out.squeeze()
    