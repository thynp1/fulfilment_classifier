import pandas as pd
import numpy as np
import time
import requests
import ast

def REEL_json_request(order, missing_value, exclude_missing=False):
    '''
    Function convert each row of a Data Frame into APPLE input json
    
    Input:
    ------
    order: a dataframe of shape (1, n+) where n is the number of variables included in the request
    missing_values: value to use to replace missing values in order
    exclude_missing: if True, sends the request without variables that are equal to np.nan
    
    Output:
    -------
    JSON request
    '''

    jr = {
      "order_id": int(order['order_id']),
      "request_setting": {
                "current_time":str(order['timeview']),
                "gps_from_redis": 1}
    }
    return jr

def _get_driver(order, url='http://3.19.67.84:6525/order/driver_suggestions'):
    """
    Get a price for an order from APPLE API
    
    Input:
    ------
    order: order input Data Frame shape (1, n+)
    url: API URL, default is 'http://3.19.67.84:6525/order/demand'
    
    Output:
    -------
    Price of an order
    """
    try:
        r = requests.post(url, json=REEL_json_request(order, 0)).content
    except:
        return np.nan
    try:
        result = ast.literal_eval(r.decode('utf-8'))['results']
        return result
    except:
        return np.nan

def get_Reel(missing_prices, url='http://3.19.67.84:6525/order/driver_suggestions'):
    """
    Get a price for an Data Frame from APPLE API
    
    Input:
    ------
    missing_prices: order input Data Frame shape (m, n)
    url: API URL, default is 'http://3.19.67.84:6525/order/demand'
    
    Output:
    -------
    Price of an order
    """
    prices = []

    for i in range(len(missing_prices)):
        order = missing_prices.iloc[i,:]
        prices.append(_get_driver(order, url))
        if i%1 == 0:
            print(f'{time.ctime(int(time.time()))}: predicted {i} orders')
    return prices
