import numpy as np
import random
import sys
import os
import json
import psycopg2
from functools import reduce
import pandas as pd
import redis
from math import radians
from sklearn.metrics import mean_squared_error, mean_absolute_error
import matplotlib as plt
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import math
from datetime import date, datetime
from statsmodels.tsa.stattools import grangercausalitytests
maxlag=12
test = 'ssr_chi2test'
today = date.today().strftime("%m-%d-%Y") 

def count_nulls(df, col_list = None):
    
    '''
    prints percentage of missing value for each column in df or for specified list of column names in col_list
    '''
    
    if col_list is not None:
        cols = col_list
    else:
        cols = df.columns
    for col in cols:
        print(f'{col}: {round(df[col].isna().sum()*100/len(df), 2)} %')
        
# connect to sql database
def connect(user, password, host, port, database): 
    return psycopg2.connect(user=user,
                            password= password,
                            host=host,
                            port=port,
                            database=database)

#Query raw
def query_raw_data(connection):
    data = pd.read_sql("""with sample as (select distinct n.id as noti_id, n.notifiable_id as order_id, target_id as driver_id, source, b.id as bid_id
, b.order_id as bid_order, b.price, b.unit_price, b.created_at as bid_created_at, b.is_counter_bid, b.status as bid_status
from public.notifications n
left join public.biddings b on b.order_id = n.notifiable_id and b.driver_id = n.target_id
where key = 'order.create' and notifiable_type = 'Order' and target_type = 'Driver'
and target_id in ( select distinct id 
                    from public.drivers
                      where bid_times_count >= 5))
                        
select distinct s.*, o.truck_model_id, o.truck_quantity, o.truck_type_id, o.notes,
        o.created_at as order_created_at, o.cargo_types, o.cargo_weight, o.cargo_length, o.cargo_width, o.cargo_height, o.is_whole_truck, o.order_type,o.pickup_datetime as pickup_date, o.dropoff_datetime as drop_date,
        d.origin_area_id as pickup_area_id, d.destination_area_id as drop_area_id,concat(d.origin_area_id, '-', d.destination_area_id,'-', o.truck_model_id) as route,
        d.duration_value as duration, d.distance_value as distance, r.distance_value as reel_score
        from sample s left join public.reel_results r on (s.order_id = r.order_id and s.driver_id = r.driver_id)
                        join public.orders o on s.order_id = o.id
                        join public.order_distance_data d on s.order_id = d.order_id 
        where d.distance_value > 0
        and o.status > 3
        and o.order_type != 'enterprise'
        and o.notes !~* 'test|tesst|lgv|%thử%|logivan|check'
        and o.cargo_weight > 0

                             """,connection)
    #closing database connection.
    if(connection):
        connection.close()
        print("Database connection is closed")
#     path = 'raw_data_bid-' + datetime.today + '.csv'
#     data.to_csv('raw_data_bid.csv',index = False)
    return data

def distance_segment(df):
    if (df['distance'] < 50e3):
        return 50
    elif (df['distance'] < 300e3):
        return 300
    elif (df['distance'] < 700e3):
        return 700
    elif (df['distance'] < 1000e3):
        return 1000
    elif (df['distance'] < 1500e3):
        return 1500
    elif (df['distance'] < 1800e3):
        return 1800
    else:
        return 1801

def cargo_segment(df):
    if (df['cargo_weight'] < 5):
        return 5
    elif (df['cargo_weight'] < 10):
        return 10
    elif (df['cargo_weight'] < 15):
        return 15
    elif (df['cargo_weight'] < 20):
        return 20
    elif (df['cargo_weight'] < 25):
        return 25
    elif (df['cargo_weight'] < 30):
        return 30
    else:
        return 31
    
def quantile_10(x):
    return np.quantile(x, q=0.1)
def quantile_90(x):
    return np.quantile(x, q=0.9)
def quantile_05(x):
    return np.quantile(x, q=0.05)
def quantile_95(x):
    return np.quantile(x, q=0.95)

def mean_squared_error(y, yhat):
    return np.mean((y - yhat)**2)


def mean_absolute_error(y, yhat):
    return np.mean(np.abs((y - yhat)))

def mape(y, yhat):
    return np.abs(((yhat - y) / y)).mean() * 100

def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100
        

def grid_histogram(pdf, ls_cname, ncols = 3):
    """
    Vẽ nhiều histogram cho các thuộc tính được xếp vào grid
    Cho số lượng grid column, ta sẽ fill out histogram cho từng cell của grid
    """
    
    # tính số dòng cần cho grid
    n_cat = len(ls_cname)    
    nrows = int(math.ceil(n_cat * 1.0 / ncols))

    # khởi tạo figure gồm nrows * ncols cho grid
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(15, 4 * nrows))
    
    # dùng tuỳ chọn này để các chart được rời nhau
    fig.set_tight_layout(False)
    
    # fill out grid
    for i in range(nrows):
        for j in range(ncols):
            
            # xác định vị trí tên column trong danh sách dựa vào (i, j, ncols)
            idx = i * ncols + j
            
            # khi plot hết thì dừng
            if idx == n_cat:
                break
                
            cname = ls_cname[idx]
            s00 = pdf[~pdf[cname].isna()][cname]
            s00.plot(kind="hist", ax=axes[i][j], rot=45, title=cname)
            
    plt.tight_layout()
    plt.show()       


def grangers_causation_matrix(data, variables, test='ssr_chi2test', verbose=False):    
    """Check Granger Causality of all possible combinations of the Time series.
    The rows are the response variable, columns are predictors. The values in the table 
    are the P-Values. P-Values lesser than the significance level (0.05), implies 
    the Null Hypothesis that the coefficients of the corresponding past values is 
    zero, that is, the X does not cause Y can be rejected.

    data      : pandas dataframe containing the time series variables
    variables : list containing names of the time series variables.
    """
    df = pd.DataFrame(np.zeros((len(variables), len(variables))), columns=variables, index=variables)
    for c in df.columns:
        for r in df.index:
            test_result = grangercausalitytests(data[[r, c]], maxlag=maxlag, verbose=False)
            p_values = [round(test_result[i+1][0][test][1],4) for i in range(maxlag)]
            if verbose: print(f'Y = {r}, X = {c}, P Values = {p_values}')
            min_p_value = np.min(p_values)
            df.loc[r, c] = min_p_value
    df.columns = [var + '_x' for var in variables]
    df.index = [var + '_y' for var in variables]
    return df

def is_shipment(df):
    if (df['shipment_id'] > 1 or df['evan_shipment_id'] > 1)&(df['driver_id'] != 0)&(df['bidding_status'] == 1):
        return 1
    else:
        return 0
