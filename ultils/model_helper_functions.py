import numpy as np
import random
import pandas as pd
import copy
from sklearn.metrics import mean_squared_error, mean_absolute_error, f1_score, accuracy_score, precision_score
from sklearn.model_selection import train_test_split
import matplotlib as plt
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import math
import torch
import time 
from datetime import date, datetime
today = date.today().strftime("%m-%d-%Y")
today_time = date.today().strftime("%m-%d-%Y") + '-' + datetime.now().strftime("%H:%M")
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
from torch.utils import data
from bayes_opt import BayesianOptimization
from bayes_opt.util import load_logs
from ultils.custom_model import *

class RegressionColumnarDataset(data.Dataset):
    def __init__(self, df, cats, y):
        self.dfcats = df[cats]
        self.dfconts = df.drop(cats, axis=1)
        self.cats = np.stack([c.values for n, c in self.dfcats.items()], axis=1).astype(np.int64)
        self.conts = np.stack([c.values for n, c in self.dfconts.items()], axis=1).astype(np.float32)
        self.y = y.values.astype(np.int64)
    def __len__(self): return len(self.y)

    def __getitem__(self, idx):
        return [self.cats[idx], self.conts[idx], self.y[idx]]

    
def load_checkpoint(filepath):
    checkpoint = torch.load(filepath)
    model = checkpoint['model']
    model.load_state_dict(checkpoint['state_dict'])
    for parameter in model.parameters():
        parameter.requires_grad = False
    
    model.eval()
    
    return model

def predict_model(dl, model, device='cpu'):
    cat_order, cont_order =  next(iter(dl))[0], next(iter(dl))[1] 
    num_order = cont_order.to(device)
    cat_order = cat_order.to(device)
    return (model(cat_order, num_order))

def setup_dataloader(X, y, batch_size=1024, shuffle=False, num_workers=4):
    catf, _ = split_features(X)
    X_dataset = RegressionColumnarDataset(X, catf, y)
    params = {
        'batch_size': batch_size,
        'shuffle': shuffle,
        'num_workers': num_workers}
    X_dataloader = data.DataLoader(X_dataset, **params)
    return X_dataloader

def setup_nn_params(df):
    catf, _ = split_features(df)
    cat_sz = [(c, int(df[c].max()+1)) for c in catf]
    emb_szs = [(c, min(50, (c+1)//2)) for _,c in cat_sz]
    n_cont = len(df.columns)-len(catf)
    return {'emb_szs': emb_szs, 'n_cont': n_cont}


def split_features(df): 
    catf = ['truck_model', 'truck_quantity', 'is_whole_truck', 'truck_type_id',
       'pickup_area', 'drop_area', 'pickup_date_year', 'pickup_date_month',
       'pickup_date_dow', 'pickup_date_quarter', 'pickup_date_isweeknd',
       'pickup_date_isholiday', 'pickup_date_month_interval',
       'cargo_weight_level', 'cargo_classifier', 'distance_level', 'distance_seg', 'cargo_seg', 'route_area']

    numf = [col for col in df.columns if col not in catf]
    return catf, numf

def to_numpy(x_tensor):
    """
    Convert a Pytorch tensor to numpy
    """
    return x_tensor.cpu().data.numpy()


def train_loop(train_dl, val_dl, emb_szs, n_cont, emb_drop, out_sz, szs, drops,seed,
               learning_rate, step_size, gamma, weight_decay, class_weight_0 = 2.047, class_weight_1 = 8.867,
               epoches=3, verbose=True):

    torch.cuda.current_device()
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    if verbose: print('Training on: ', device)
    torch.manual_seed(seed)
    print("starting")
    model = NNModel(emb_szs=emb_szs, 
            n_cont=n_cont, 
            emb_drop=emb_drop, 
            out_sz= out_sz, 
            szs=szs, 
            drops=drops).to(device)

    model.apply(customize_weight_init)
    class_weights = torch.FloatTensor([class_weight_0, class_weight_1]).to(device)
    opt = torch.optim.Adam(model.parameters(),lr = learning_rate, weight_decay=weight_decay)
    loss_fn = nn.CrossEntropyLoss(weight = class_weights, reduction='sum')
    scheduler=torch.optim.lr_scheduler.StepLR(opt, step_size=int(step_size), gamma= gamma, last_epoch=-1)
    # Some setup
    accuracy = []
    f1_scores = []
    best_epoch = 0
    start_time = time.time()
    num_batch = len(train_dl)
    cats_tensor = torch.LongTensor(val_dl.dataset.cats).to(device)
    conts_tensor = torch.FloatTensor(val_dl.dataset.conts).to(device)
    y_true = val_dl.dataset.y
    for epoch in range(epoches):      
        total_loss_train = 0 
        train_loss = 0
        model.train()
        scheduler.step()
        for cat, cont, y in iter(train_dl):
            cat = cat.to(device)
            cont = cont.to(device)
            y = y.to(device)
            opt.zero_grad()
            prob = model(cat, cont).to(device)
            loss = loss_fn(prob, y)
            loss.backward()
            opt.step()
            total_loss_train += loss.item()
        train_loss = total_loss_train/num_batch
        
        model.eval()
        val_prob = model(cats_tensor, conts_tensor).to(device)
        _ , predicted_class = torch.max(val_prob.data, dim = 1)
        f1 = f1_score(y_true, to_numpy(predicted_class))
        precision = precision_score(y_true, to_numpy(predicted_class))
        f1_scores.append(f1)
        if verbose:
            cur_lr = opt.param_groups[0]['lr']
            print(f'Epoch {epoch+1}: train_loss: {train_loss:.4f} val_f1: {f1:.4f} val_precision: {precision:.4f} current_lr: {cur_lr: .5f}')

        if f1_scores[-1] == max(f1_scores):
            best_model= copy.deepcopy(model)
            best_scheduler = copy.deepcopy(scheduler)
            best_epoch= epoch + 1
            best_accuracy = round(max(f1_scores),2)
            best_optimizer=copy.deepcopy(opt)
            checkpoint = {
                'model': NNModel(emb_szs=emb_szs, n_cont=n_cont, emb_drop=emb_drop, out_sz= out_sz, szs=szs, drops=drops),
                'epoch': epoch + 1,
                'state_dict': best_model.state_dict(),
                'scheduler': best_scheduler.state_dict(),
                'optimizer': best_optimizer.state_dict(),
                'best_accuracy': best_accuracy}
            path_checkpoint =  'model/embedding_classifier_crossentropy_checkpoint_' + today_time + '.pth'
            torch.save(checkpoint, path_checkpoint)

    if verbose:
        print(f"Finished training in {time.time() - start_time:.4f} seconds")
        print('Need {} epoches to reach the best model'.format(best_epoch))
    return best_accuracy