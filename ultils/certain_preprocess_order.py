import os
import sys
sys.path.append(os.getcwd())
from ultils.preprocess_utils import *
from ultils.general_helper_functions import *
import fasttext
from pyvi import ViTokenizer
import argparse

def df_apple_preprocessor(df):
    # Process is_whole_truck
    df['is_whole_truck'] = df['is_whole_truck'].astype(int)
    
    df['timeview'] = pd.to_datetime(pd.to_datetime(df['order_created_at']).apply(lambda x: x.strftime('%Y-%m-%d')))
    # time dif between pickup and drop
    df['delivery_spd'] = pd.to_datetime(df['drop_date']) - pd.to_datetime(df['pickup_date'])
    df['delivery_spd'] = df['delivery_spd'].fillna(pd.to_timedelta(7, unit='day'))
    df['delivery_spd'] = df['delivery_spd'].apply(lambda x: x.total_seconds())
    # if it is same day delivery, set it to 12hours
    df['delivery_spd'] = df['delivery_spd'].replace(0, 12 * 3600)  
    df['delivery_spd'] = df['duration'] / df['delivery_spd']

    # Feature engineering datetime
    df = process_datetime(df, 'pickup_date')

    # weight of goods per truck
    df['cargo_weight_per_truck'] = df['cargo_weight'] / df['truck_quantity']

    # weight usage
    df['cargo_weight_per_truck'] = (df['truck_weight']**(df['is_whole_truck'])) * (
        df['cargo_weight_per_truck']**(1 - df['is_whole_truck']))

    # discount by volume of goods
    df['cargo_weight_level'] = df['cargo_weight_per_truck'] // 5

    # Preprocessing text
    # Convert text to lowercase
    df['cargo_types'] = df['cargo_types'].str.lower()
    # Remove punctuation
    df['cargo_types'] = df['cargo_types'].str.replace(r'[^\w\s]','')
    df['cargo_types'] = df['cargo_types'].str.strip()  # Remove whitespaces
    # Tokenize
    df['cargo_types'] = df['cargo_types'].apply(lambda x: ViTokenizer.tokenize(str(x)))  

    # Predict
    prediction = fasttext.load_model('model/cargo_classifier.bin').predict(df['cargo_types'].tolist())
    df['cargo_classifier'] = [0 if pred[0] == '__label__0' else 1 for pred in prediction[0]]
    # Speed
    df['distance_level'] = df.apply(distance_level, axis=1)
    df['speed'] = df['distance'] / df['duration']
    df['speed'] = df.apply(adjust_speed, axis=1)

    # Polynomial features
    df['truck_weight/duration'] = df['truck_weight'] / df['duration']
    df['truck_weight/distance'] = df['truck_weight'] / df['distance']
    df['fuel_pricexdistance'] = 20 * (df['distance'] / 1e3)
    df['durationxspeed'] = df['duration'] * df['speed']
    df['durationxdistance'] = df['duration'] * df['distance']
    df['cargo_weight_per_truckxdistance'] = df['cargo_weight_per_truck'] * df['distance']
    df['cargo_weight_per_truckxduration'] = df['cargo_weight_per_truck'] * df['duration']

    # ratio between cargo weight and truck weight, if truck_weight = 0, then this ratio = 0
    df['cargo_weight_per_truck/truck_weight'] = df['cargo_weight_per_truck'] / df['truck_weight']
    df['cargo_weight_per_truck/truck_weight'].replace(np.inf, 0, inplace=True)
    return df

def df_certain_preprocessor(df):
    df['distance_seg'] = df.apply(distance_segment, axis=1)
    df['cargo_seg'] = df.apply(cargo_segment, axis=1)
    df['segment'] = df['distance_seg'].astype(str) + '-' + df['cargo_seg'].astype(str)

    df['cargo_weight_level'] = df['cargo_weight_level'].apply(lambda x: min(x,516))
    df['distance_level'] = df['distance_level'].apply(lambda x: min(x,8))
    # Create feature illustrating route from pickup_area to drop_area
    df['route_area'] = (df['pickup_area'].astype('int')).astype(str) + (df['drop_area'].astype('int')).astype(str)
    df['pickup_freq'] = df.apply(pickup_freq, axis=1)
    df['drop_freq'] = df.apply(drop_freq, axis=1)
    df = add_truck_volume(df)
    df['cargo_volume'] = df['cargo_length'] * df['cargo_width'] * df['cargo_height']
    df['cargo_volume/truck_volume'] = df['cargo_volume'] / df['truck_volume']
    df['cargo_volume/truck_volume'] = df['cargo_volume/truck_volume'].replace(np.nan, 0)
    df['cargo_volume/truck_volume'] = df['cargo_volume/truck_volume'].replace(np.inf, 0)
    
    df['price'] = (df['apple_price'] / df['truck_quantity']) / 1e6
    # price per km
    df['price_per_km'] = df['price'] / (df['distance'] / 1e3)
    # price per km per ton
    df['price_per_km_per_ton'] = (df['price'] / (df['distance'] / 1e3)) / df['cargo_weight_per_truck']
    df['pricexduration'] = df['price']*df['duration']
    df['pricextruck_weight'] = df['price']*df['truck_weight']
    df['pricexdistance'] = df['price']*df['distance']
    df['pricexcargo_weight_per_truck'] = df['price']*(df['cargo_weight']/df['truck_quantity'])
    df['pricextruck_weightxdistance'] = df['price']*df['truck_weight']*df['distance']
    df['pricexdurationxspeed'] = df['price']*df['duration']*df['speed']
    df['pricexdurationxdistance'] = df['price']*df['duration']*df['distance']
    df['pricexcargo_weightxdistance'] = df['price']*df['cargo_weight']*df['distance']
    df['pricexcargo_weightxdurationxdistance'] = df['price']*df['cargo_weight']*df['duration']*df['distance']
    df['price_per_kmxcargo_weight'] = df['price_per_km']/df['cargo_weight']
    df['price_per_second'] = df['price']/df['duration']
    df['price_per_cargo_weight'] = df['price']/df['cargo_weight']
    df['price_per_ton'] = df['price']/df['truck_weight']
    return df

def certain_preprocessor_full(df):
    
    df = apple_fill_missing(df)
    
#     df = certain_fill_missing(df)

    df = df_apple_preprocessor(df)

    df = df_certain_preprocessor(df)

    return df
#=============DEFINE INPUT AND EXECUTE FILE===========

def get_args():

    parser = argparse.ArgumentParser('Return preprocessed DataFrame for CERTAIN')

    parser.add_argument('--data_dir',
        type=str,
        default = 'data/data_certain_driver_remove_outlier-02-25-2020.csv',
        help='(required) specify file path and file name for input data'
        )

    parser.add_argument('--output_dir',
        type=str,
        default= 'temp_data/data_certain_driver_preprocessed-' + today + '.csv',              
        help='(optional) specify file path and file name for output data, defaul format is "temp_data/data_certain_driver_preprocessed-[date].csv"'
        )

    args = parser.parse_args()

    return args

def preprocess_and_save(args):

    df = pd.read_csv(args.data_dir)
    print(f'Data shape: {df.shape}')
    print(df.columns)
    target = (df['bid_price'] / df['truck_quantity']).copy()
    del df['bid_price']

    print('Start preprocessing...')

    start_time = time.time()

    rename_map = {
        'cargo_type':'cargo_types',
        'model_price':'bid_price'
    }

    df.rename(columns = rename_map, inplace=True)

    df = certain_preprocessor_full(df)

    print(f'Done preprocessing in {time.time()-start_time} seconds')
    print(f'Data shape after preprocess: {df.shape}')
    df['driver_price'] = target
    print(f'Save file to {args.output_dir}')
    df.to_csv(args.output_dir, index=False)

if __name__ == '__main__':

    args = get_args()

    preprocess_and_save(args)