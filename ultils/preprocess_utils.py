import pandas as pd 
import numpy as np 
import os
import sys
sys.path.append(os.getcwd())
from ultils.general_helper_functions import *
import warnings
warnings.filterwarnings("ignore")
from subprocess import check_output
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import FunctionTransformer
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import FeatureUnion
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression
import matplotlib
import matplotlib.pyplot as plt
from sklearn.svm import SVR
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import reciprocal, uniform

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from xgboost import XGBClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score, precision_score

def get_days_in_month(d):
    """
    Function to categorize days in month
    categorize days to three period 1-10, 11-20, and 20-31
    """
    if d <11: 
        return 'first_10'
    elif 11<=d<=20: 
        return '10_to_20'
    else:
        return 'after 20'
    
def process_datetime(df,feature):
    vn_holiday=['2020-01-01','2020-01-24','2020-01-25','2020-01-26','2020-01-27',
                '2020-01-28','2020-01-29','2020-02-14','2020-03-20','2020-04-02',
                '2020-04-30','2020-05-01','2020-06-21','2020-06-28','2020-09-02',
                '2020-09-22','2020-10-20','2020-10-31','2020-12-21','2020-12-24',
                '2020-12-25','2020-12-25', # 2020 holidays
                '2019-01-01','2019-01-02','2019-01-03','2019-02-03','2019-02-04','2019-02-05',
                '2019-02-06','2019-02-07','2019-02-08','2019-02-09','2019-02-10','2019-02-14',
                '2019-04-13','2019-04-14','2019-04-30','2019-05-01','2019-08-31','2019-09-01',
                '2019-09-02','2019-10-20','2019-10-31','2019-12-24','2019-12-25','2019-12-31' # 2019 holidays
               ]
    df[feature+'_year'] = pd.to_datetime(df[feature]).dt.year
    df[feature+'_month'] = pd.to_datetime(df[feature]).dt.month
    df[feature+'_dow'] = pd.to_datetime(df[feature]).dt.weekday
    df[feature+'_quarter'] = pd.to_datetime(df[feature]).dt.quarter
    df[feature+'_isweeknd'] = [0 if int(d)<5 else 1 for d in df[feature+'_dow']]
    df[feature+'_isholiday']=[1 if str(d) in vn_holiday else 0 for d in pd.to_datetime(df[feature]).dt.date]
    df[feature+'_month_interval']=[get_days_in_month(d.day) for d in pd.to_datetime(df[feature])]
    return df

def distance_level(df):
    if df['distance'] <= 5e4:
        return 1
    elif df['distance'] <= 1e5:
        return 2
    elif df['distance'] <= 2e5:
        return 3
    elif df['distance'] <= 3e5:
        return 4
    elif df['distance'] <= 5e5:
        return 5
    elif df['distance'] <= 1e6:
        return 6
    else:
        return 7

def adjust_speed(df):
    if df['speed'] > 11.5:
        return 11.5
    else:
        return df['speed']

def fix_length(df):
    """
    Fix wrong input of cargo length
    Maximum length of truck is 14.456m so 
    if cargo length < 15: then keep cargo lenght
    else 15 < cargo length < 1500, divide cargo length by 100
    else divide cargo length by 1000
    """
    if df['cargo_length'] <= 15:
        return df['cargo_length']
    elif (df['cargo_length'] > 15) & (df['cargo_length'] < 1500):
        return df['cargo_length'] / 100
    else:
        return df['cargo_length'] / 1000
    
def fix_width(df):
    """
    Fix wrong input of cargo width
    Maximum width of truck is 2.4m so 
    if cargo width < 2.4: then keep cargo lenght
    else 2.4 < cargo width < 2400, divide cargo width by 100
    else divide cargo width by 1000
    """
    if df['cargo_width'] <= 2.4:
        return df['cargo_width']
    elif (df['cargo_width'] > 2.4) & (df['cargo_width'] < 240):
        return df['cargo_width'] / 100
    else:
        return df['cargo_width'] / 1000
        
def fix_height(df):
    """
    Fix wrong input of cargo height
    Maximum height of truck is 2.7m so 
    if cargo height < 2.7: then keep cargo lenght
    else 2.7 < cargo height < 2700, divide cargo height by 100
    else divide cargo height by 1000
    """
    if df['cargo_height'] <= 2.7:
        return df['cargo_height']
    elif (df['cargo_height'] > 2.7) & (df['cargo_height'] < 270):
        return df['cargo_height'] / 100
    else:
        return df['cargo_height'] / 1000

def region_code_mapping(zone_id):
    if (zone_id in [65, 66]):
        return 64
    if (zone_id in [68, 69, 70]):
        return 67
    return -1
    
def pickup_freq(df):
    pickup_freq = load_pickle('data/pickup_dict-02-22-2020.pkl')['freq']
    if df['pickup_area'] in pickup_freq:
        return float(pickup_freq[df['pickup_area']])
    return 0

def drop_freq(df):
    drop_freq = load_pickle('data/drop_dict-02-22-2020.pkl')['freq']
    if df['drop_area'] in drop_freq:
        return float(drop_freq[df['drop_area']])
    return 0

def add_truck_volume(df):
    truck_model = pd.read_csv('data/truck_models.csv')
    volume = truck_model[['truck_model', 'truck_volume']]
    df = df.merge(volume, how='left', on='truck_model', right_index=True)
    df['truck_volume'] = df['truck_volume'].fillna(0)
    return df

def apple_fill_missing(df):
    columns_fill_NA_with_0 = [
        'truck_type_id',
        'cargo_weight',
        'cargo_length',
        'cargo_width',
        'cargo_height',
        'truck_weight',
    ]
    df[['cargo_length', 'cargo_width', 'cargo_height', 'truck_weight']] = df[['cargo_length', 'cargo_width', 'cargo_height', 'truck_weight']].fillna(0)
    df['cargo_types'] = df['cargo_types'].fillna('Hàng thường')
    df['is_tax_invoice_requested'] = df['is_tax_invoice_requested'].fillna(True)
    df['is_whole_truck'] = df['is_whole_truck'].fillna(True)
    df['bidder_type'] = df['bidder_type'].fillna(1)
    return df

def certain_fill_missing(df):
    df['duration'] = df['duration'].fillna(df['distance']/14.3)
    df['order_type'] = df['order_type'].fillna("marketplace")
    df['platform'] = df['platform'].fillna("ios")
    return df

def save_pickle(filename, model):
    with open(filename, 'wb') as f:
        pickle.dump(model, f)

def load_pickle(path):
    with open(path,'rb') as f:
        return pickle.load(f)
    
def pairplot_custom(data,cols, alpha, hue):
    if alpha == 1:
        data['is_higher'] = [1 if d >= alpha else 0 for d in data[hue]]
        data = data.fillna(0)
        sns.pairplot(data, hue = 'is_higher', vars = cols)
        plt.show()
    else:
        data = data[data[hue] > 0]
        data['is_higher'] = [1 if d > alpha else 0 for d in data[hue]]
        data = data.fillna(0)
        sns.pairplot(data, hue = 'is_higher', vars = cols)
        plt.show()
def create_metric_perfrom(df):
    df['mae'] = (df['actual'] - df['predicted'])
    df['rmse'] = np.sqrt((df['actual'] - df['predicted'])**2)
    return df

## https://pandas.pydata.org/pandas-docs/stable/categorical.html
def remove_categories(df1, df2, cat_cols):
    for i in cat_cols:
        a = df1[i].astype('category')
        a = a.cat.categories
    
        b = df2[i].astype('category')
        b = b.cat.categories
    
        ## That which is in train which is not in test
        d = list(set(a) - set(b))
    
        ## Therefore these need to be removed from train
        for j in d:
            df1.drop(df1[df1[i] == j].index, inplace = True)
            print("{} removed from {} in df1".format(j,i))
            
    return(df1)

class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.attribute_names].values
    
class CatFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        Z = pd.get_dummies(X[self.attribute_names], drop_first = False)
        return Z.values